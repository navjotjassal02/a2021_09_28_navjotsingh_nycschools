package com.navjotsingh.a2021_09_28_navjotsingh_nycschools.View

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Models.Constants
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Presenter.SatScorePresenter
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Presenter.SchoolContract
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.R
import kotlinx.android.synthetic.main.activity_sat_score.*
import kotlinx.android.synthetic.main.not_found.view.*

class SatScoreActivity : AppCompatActivity(), SchoolContract.SatScore {

    internal var dbnId: String? = null
    var satScorePresenter: SatScorePresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sat_score)
        // get Intent data after click on school
        dbnId = intent.getStringExtra(Constants.DBN_ID)
        setTitle(getString(R.string.display_sat_title))
        init()
    }

    private fun init() {
        satScorePresenter = SatScorePresenter(this, dbnId)
        satScorePresenter!!.getSatScores()
    }

    // setting a data to the view
    override fun onSatScore(satTakers: String?, schoolName: String?, math: String?, reading: String?, writing: String?) {
        txt_header.text = schoolName
        txt_total.text = getString(R.string.sat_title) + "  " +satTakers
        txt_math.text = getString(R.string.math_scrore_title) + "  " + math
        txt_reading.text = getString(R.string.reading_score_title) + "  " + reading
        txt_writing.text = getString(R.string.writing_score_title) + "  " + writing
    }

    // Show Empty Screen selected school dont have any details
    override fun onEmptyState() {
        cardview.visibility = View.GONE
        layout_notFound.visibility = View.VISIBLE

        layout_notFound.btn_home.setOnClickListener {
            finish()
        }
    }

    override fun onError() {
        Toast.makeText(this@SatScoreActivity, getString(R.string.error_message), Toast.LENGTH_SHORT).show()
    }

}
