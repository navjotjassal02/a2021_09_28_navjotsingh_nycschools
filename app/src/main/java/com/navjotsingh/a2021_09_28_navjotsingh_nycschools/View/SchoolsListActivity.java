package com.navjotsingh.a2021_09_28_navjotsingh_nycschools.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Adapter.SchoolAdapter;
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Models.Constants;
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Models.SchoolModel;
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Presenter.SchoolContract;
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Presenter.SchoolListPresenter;
import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.R;

import java.util.ArrayList;
import java.util.List;

public class SchoolsListActivity extends AppCompatActivity implements SchoolContract.School, SchoolContract.AddItemListener {
    private RecyclerView recyclerView;
    private List<SchoolModel> getschoolModelList;
    private ProgressBar progressBar;
    private SchoolAdapter adapter;
    private EditText txt_enter_city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools_list);
        setTitle(Constants.SCHOOL_TITLE);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SchoolListPresenter schoolListPresenter = new SchoolListPresenter(this);
        schoolListPresenter.getSchoolList();
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerview_school_list);
        progressBar = findViewById(R.id.progress_bar);
        txt_enter_city = findViewById(R.id.input_field);
        getschoolModelList = new ArrayList<>();

        txt_enter_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });
    }


    @Override
    public void onSchoolList(List<SchoolModel> schoolModelList) {
        getschoolModelList = schoolModelList;
        adapter = new SchoolAdapter(this, schoolModelList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SchoolsListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(SchoolsListActivity.this, getString(R.string.error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClickListener(SchoolModel schoolModel) {
        Intent intent = new Intent(this, SatScoreActivity.class);
        intent.putExtra(Constants.DBN_ID, schoolModel.getDbn());
        startActivity(intent);
    }


    private void filter(String text) {
        if (getschoolModelList.size() > 0) {
            ArrayList<SchoolModel> filteredSchoolList = new ArrayList<>();
            for (SchoolModel schoolModel : getschoolModelList) {
                if (schoolModel.getSchoolName().toLowerCase().contains(text.toLowerCase())) {
                    filteredSchoolList.add(schoolModel);
                }
            }
            adapter.filterList(filteredSchoolList);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        txt_enter_city.getText().clear();
    }
}
