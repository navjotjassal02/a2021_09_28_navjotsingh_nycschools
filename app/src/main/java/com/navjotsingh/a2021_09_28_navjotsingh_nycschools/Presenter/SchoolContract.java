package com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Presenter;

import com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Models.SchoolModel;

import java.util.List;

public class SchoolContract {

    public interface School {
        void onSchoolList(List<SchoolModel> schoolModelList);

        void onError();

        void onProgress();
    }

    public interface AddItemListener {
        void onClickListener(SchoolModel schoolModel);
    }

    public interface SatScore {

        void onSatScore(String satTakers, String schoolName,  String math, String reading, String writing);

        void onError();

        void onEmptyState();
    }
}
