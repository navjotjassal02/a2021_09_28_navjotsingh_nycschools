package com.navjotsingh.a2021_09_28_navjotsingh_nycschools.Models;

import android.app.ProgressDialog;
import android.content.Context;

public class Constants {
    public static final String DBN_ID = "dbn";
    public static final String SCHOOL_TITLE = "Schools List";
    public static final String NA = "N/A";
    public static final String SAT_SCORE_JSON = "{\"dbn\":\"01M292\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\"}";
    public static final String SCHOOL_LIST_JSON = "{\"dbn\":\"02M260\",\"school_name\":\"Clinton School Writers & Artists, M.S. 260\",\"city\":\"Manhattan\",\"zip\":\"10003\",\"state_code\":\"NY\"}";
}
